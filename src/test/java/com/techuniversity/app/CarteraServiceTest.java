package com.techuniversity.app;

import com.techuniversity.app.models.CarteraModel;
import com.techuniversity.app.models.MonedaModel;
import com.techuniversity.app.services.CarteraService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class CarteraServiceTest {

    @Autowired
    CarteraService carteraService;

    @Test
    public void addCarteraTest(){
        CarteraModel carteraModel = new CarteraModel();
        carteraModel.setMonedas(new ArrayList<MonedaModel>());
        MonedaModel monedaModel = new MonedaModel("NES", 10, 15);
        carteraService.addCartera(carteraModel, monedaModel);
        System.out.println(carteraModel.toString());
        assertTrue(carteraModel.getMonedas().size()>0);
        assertEquals(carteraModel.getMonedas().get(0).getMoneda().toString(),monedaModel.getMoneda().toString());
    }

    @Test
    public void deleteCarteraTest(){
        CarteraModel carteraModel = new CarteraModel();
        carteraModel.setMonedas(new ArrayList<MonedaModel>());
        MonedaModel monedaModel1 = new MonedaModel("VGP", 1, 5);
        MonedaModel monedaModel2 = new MonedaModel("NES", 10, 15);
        carteraService.addCartera(carteraModel, monedaModel1);
        carteraService.addCartera(carteraModel, monedaModel2);
        System.out.println(carteraModel.toString());
        carteraService.deleteCartera(carteraModel, monedaModel1);
        System.out.println(carteraModel.toString());
        assertTrue(carteraModel.getMonedas().size()==1);
        assertEquals(carteraModel.getMonedas().get(0).getMoneda().toString(),monedaModel2.getMoneda().toString());
    }
    @Test
    public void addCarteraNullTest(){
        CarteraModel carteraModel = new CarteraModel();
        //carteraModel.setMonedas(new ArrayList<MonedaModel>());
        assertTrue(carteraModel.getMonedas() == null);
        MonedaModel monedaModel1 = new MonedaModel("VGP", 1, 5);
        carteraService.addCartera(carteraModel, monedaModel1);
        assertTrue(carteraModel.getMonedas().size()==1);
    }
}
