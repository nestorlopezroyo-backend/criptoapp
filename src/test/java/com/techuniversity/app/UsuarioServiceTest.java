package com.techuniversity.app;

import com.techuniversity.app.enums.Rol;
import com.techuniversity.app.models.CarteraModel;
import com.techuniversity.app.models.MonedaModel;
import com.techuniversity.app.models.UsuarioModel;
import com.techuniversity.app.services.UsuarioService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class UsuarioServiceTest {

    @Autowired
    UsuarioService usuarioService;

    @Test
    public void userTest(){
        //Alta usuarioMOdel
        UsuarioModel user = new UsuarioModel();
        user.setUsuario("Juan65");
        user.setRol(Rol.CLIENTE);
        user.setCartera(new CarteraModel(new ArrayList<MonedaModel>()));
        //Escribimos usuario en mongo
        usuarioService.save(user);
        UsuarioModel userConsultado = usuarioService.findByUsuario(user.getUsuario());
        //Validamos si encontramos por usuario
        assertEquals(userConsultado.getUsuario(),user.getUsuario());
        //Validamos si encontramos por id
        assertEquals(usuarioService.findById(userConsultado.getId()).get().getUsuario(),user.getUsuario());
        //Borramos usuario de mongo
        usuarioService.deleteUsuario(user);
        //Validamos que lo hemos borrado consultando por id
        assertTrue(!usuarioService.findById(userConsultado.getId()).isPresent());
    }
}
