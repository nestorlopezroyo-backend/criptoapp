package com.techuniversity.app.services;

import com.techuniversity.app.models.UsuarioModel;
import com.techuniversity.app.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class UsuarioService {
    @Autowired
    UsuarioRepository usuarioRepository;

    public List<UsuarioModel> findAll() {
        return usuarioRepository.findAll();
    }

    //Optional, devuelve el producto o null sino lo encuentra
    public Optional<UsuarioModel> findById(String id) {
        return usuarioRepository.findById(id);
    }

    public UsuarioModel save(UsuarioModel producto) {
        return usuarioRepository.save(producto);
    }

    public boolean deleteUsuario(UsuarioModel producto) {
        try {
            usuarioRepository.delete(producto);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public List<UsuarioModel> findPaginado(int page) {
        Pageable pageable = PageRequest.of(page, 5);
        Page<UsuarioModel> pages = usuarioRepository.findAll(pageable);
        List<UsuarioModel> productos = pages.getContent();
        return productos;
    }

    public UsuarioModel findByUsuario(String user) {
        List<UsuarioModel> arrayList = this.findAll();
        Stream<UsuarioModel> arrayList2 = arrayList.stream().filter(usuarioModel -> {
                    return usuarioModel.getUsuario().equals(user);
                }
        );
        Optional<UsuarioModel> usuario = arrayList2.findFirst();
        if (usuario.isPresent()) {
            UsuarioModel usuarioModelReturn = usuario.get();
            return usuarioModelReturn;
        }else{
            return null;
        }
    }
}
