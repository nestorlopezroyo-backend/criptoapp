package com.techuniversity.app.services;

import com.techuniversity.app.models.CarteraModel;
import com.techuniversity.app.models.MonedaModel;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CarteraService {

    public CarteraModel addCartera(CarteraModel cartera, MonedaModel moneda) {
        List<MonedaModel> arrayMonedas = cartera.getMonedas();
        if(arrayMonedas == null){
            arrayMonedas = new ArrayList<MonedaModel>();
        }
        arrayMonedas.add(moneda);
        cartera.setMonedas(arrayMonedas);
        return cartera;
    }

    public CarteraModel deleteCartera(CarteraModel cartera, MonedaModel moneda) {
        List<MonedaModel> arrayMonedas  = cartera.getMonedas();
        int pos = arrayMonedas.indexOf(moneda);
        arrayMonedas.remove(pos);
        cartera.setMonedas(arrayMonedas);
        return cartera;
    }
}
