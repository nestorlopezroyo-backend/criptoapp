package com.techuniversity.app.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Component
public class PrecioService {
    @Autowired
    private RestTemplate restTemplate;
    @Value("${token}")
    private String token;
    private static String url_coinranking = "https://api.coinranking.com/v2/coin/";

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public double recuperarPrecio(String moneda){
        double precio = connectarCoinranking(moneda);
        System.out.println("Precio: "+ precio);
        return precio;
    }

    public double connectarCoinranking(String moneda){
        HttpHeaders headers = new HttpHeaders();
        JsonNode jsonNode = null;
        double precio = 0;
        headers.add("x-access-token", token);
        String body = "";
        HttpEntity<String> request = new HttpEntity<String>(body, headers);
        System.out.println(headers);
        String url = crearUrlMoneda(moneda);
        System.out.println("URL--> "+url);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET,request,String.class);
        System.out.println("Respuesta: "+response);
        String respuesta = response.toString().substring(5);
        System.out.println("Respuesta2: "+respuesta);
        try{
            jsonNode = new ObjectMapper().readTree(respuesta);
            System.out.println("Map OK");
        }catch (IOException e){
            System.out.println("Map ERROR");
            return precio;
        }
        precio = jsonNode.findValue("price").asDouble();
        return precio;
    }

    public String crearUrlMoneda(String Moneda){
        String url ="";
        switch (Moneda){
            case "BTC":
              url = url_coinranking + "/Qwsogvtv82FCd?referenceCurrencyUuid=5k-_VTxqtCEI";
              break;
            case "ETH":
                url = url_coinranking + "/razxDUgYGNAdQ?referenceCurrencyUuid=5k-_VTxqtCEI";
                break;
            case "BNB":
                url = url_coinranking + "/WcwrkfNI4FUAe?referenceCurrencyUuid=5k-_VTxqtCEI";
                break;
            case "ADA":
                url = url_coinranking + "/qzawljRxB5bYu?referenceCurrencyUuid=5k-_VTxqtCEI";
                break;
            case "DOGE":
                url = url_coinranking + "/a91GCGd_u96cF?referenceCurrencyUuid=5k-_VTxqtCEI";
                break;
            case "XRP":
                url = url_coinranking + "/-l8Mn2pVlRs-p?referenceCurrencyUuid=5k-_VTxqtCEI";
                break;
            case "HEX":
                url = url_coinranking + "/9K7m6ufraZ6gh?referenceCurrencyUuid=5k-_VTxqtCEI";
                break;
            case "DOT":
                url = url_coinranking + "/25W7FG7om?referenceCurrencyUuid=5k-_VTxqtCEI";
                break;
            case "UNI":
                url = url_coinranking + "/_H5FVG9iW?referenceCurrencyUuid=5k-_VTxqtCEI";
                break;
            case "ICP":
                url = url_coinranking + "/aMNLwaUbY?referenceCurrencyUuid=5k-_VTxqtCEI";
                break;
            case "BCH":
                url = url_coinranking + "/ZlZpzOJo43mIo?referenceCurrencyUuid=5k-_VTxqtCEI";
                break;
        }
        return url;
    }

}
