package com.techuniversity.app.controllers;

import com.techuniversity.app.components.JWTBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("token")
//@CrossOrigin(origins = "*", methods ={ RequestMethod.GET, RequestMethod.POST})
public class LoginController {
    @Autowired
    JWTBuilder jwtBuilder;

    @GetMapping("/")
    public String tokenget(@RequestParam(value="usuario") String usuario,
                           @RequestParam(value="secret") String password,
                           @RequestParam(value="rol", defaultValue = "Cliente") String rol)
    {
        System.out.println("Genero Token con: " +usuario + " " + password +" " + rol);
        return jwtBuilder.generateToken(usuario,password,rol);
    }

}
