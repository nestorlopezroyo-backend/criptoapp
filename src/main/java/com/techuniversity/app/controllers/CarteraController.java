package com.techuniversity.app.controllers;

import com.techuniversity.app.components.JWTBuilder;
import com.techuniversity.app.models.CarteraModel;
import com.techuniversity.app.models.MonedaModel;
import com.techuniversity.app.models.UsuarioModel;
import com.techuniversity.app.services.CarteraService;
import com.techuniversity.app.services.PrecioService;
import com.techuniversity.app.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("cartera")
public class CarteraController {
    @Autowired
    CarteraService carteraService;
    @Autowired
    UsuarioService usuarioService;
    @Autowired
    PrecioService precioService;

    @Autowired
    JWTBuilder jwtBuilder;

    @GetMapping(path = "/", headers = {"Authorization"})
    public ResponseEntity<CarteraModel> getCartera (@RequestHeader("Authorization") String token){

        String usuario = recuperarUsuario(token);

        if (usuario == null){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        UsuarioModel user = usuarioService.findByUsuario(usuario);

        if (user == null){
            return ResponseEntity.notFound().build();
        }else{
            List<MonedaModel> listaMonedas = new ArrayList<>();
             listaMonedas = user.getCartera().getMonedas();
             if (listaMonedas != null) {
                 for(int i=0; i < listaMonedas.size(); i++){
//                System.out.println("Cambio antes de multiplicar" + listaMonedas.get(i).getCambio());
                     listaMonedas.get(i).setCambio(listaMonedas.get(i).getCantidad() * precioService.recuperarPrecio(listaMonedas.get(i).getMoneda()));
//                System.out.println("Cambio despues de multiplicar" + listaMonedas.get(i).getCambio());
                 }
             }
            return ResponseEntity.ok().body(user.getCartera());
        }
    }

    @GetMapping(path = "/{nombre}", headers = {"Authorization"})
    public ResponseEntity<MonedaModel> getMoneda (@RequestHeader("Authorization") String token, @PathVariable String nombre){
        String usuario = recuperarUsuario(token);
        System.out.println("moneda: " + nombre);
        if (usuario == null){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        UsuarioModel user = usuarioService.findByUsuario(usuario);
        if (user == null){
            return ResponseEntity.notFound().build();
        }else{
            System.out.println("entro for");
            for (MonedaModel moneda : user.getCartera().getMonedas()){
                System.out.println(moneda.getMoneda());
                if (moneda.getMoneda().equals(nombre)){
                    if (moneda != null){
                        moneda.setCambio(moneda.getCantidad() * precioService.recuperarPrecio(moneda.getMoneda()));
                    }
                    return ResponseEntity.ok().body(moneda);
                }
            }
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping(path = "/", headers = {"Authorization"})
    public ResponseEntity<UsuarioModel> addMonedas(@RequestHeader("Authorization") String token, @RequestBody List<MonedaModel> monedas) {

        if (monedas == null) {
            return ResponseEntity.noContent().build();
        }

        String usuario = recuperarUsuario(token);

        if (usuario == null){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        UsuarioModel user = usuarioService.findByUsuario(usuario);

        if (user == null){
            return ResponseEntity.notFound().build();
        }

        CarteraModel cartera = user.getCartera();

        for (MonedaModel moneda : monedas) {
            cartera = carteraService.addCartera(cartera, moneda);
        }
        user.setCartera(cartera);
        usuarioService.save(user);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping(path = "/", headers = {"Authorization"})
    public ResponseEntity<UsuarioModel> compraventaMoneda(@RequestHeader("Authorization") String token, @RequestBody MonedaModel nombre) {
        System.out.print(nombre.toString());
        if (nombre == null) {
            return ResponseEntity.noContent().build();
        }

        String usuario = recuperarUsuario(token);

        if (usuario == null){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        UsuarioModel user = usuarioService.findByUsuario(usuario);

        if (user == null){
            return ResponseEntity.notFound().build();
        }

        CarteraModel cartera = user.getCartera();
        System.out.println(cartera.toString());
        int j = 0;
        MonedaModel moneda = new MonedaModel();

        for (int i = 0; i < user.getCartera().getMonedas().size(); i++){
            moneda = user.getCartera().getMonedas().get(i);
            System.out.println(moneda.toString());
            if (moneda.getMoneda().equals(nombre.getMoneda())){
                if (moneda.getCantidad() + nombre.getCantidad() >= 0){
                    moneda.setCantidad(moneda.getCantidad() + nombre.getCantidad());
                    System.out.println("he sumado --> " + moneda.toString());
                    j = i;
                }else{
                    System.out.println("Cantidad a retirar superior a " + moneda.getCantidad());
                    return ResponseEntity.badRequest().build();
                }
            }
        }
        System.out.println(user.toString());
        System.out.println(cartera.toString());
        user.setCartera(cartera);
        usuarioService.save(user);
        System.out.println(user.toString());
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/", headers = {"Authorization"})
    public ResponseEntity<UsuarioModel> deleteCartera(@RequestHeader("Authorization") String token, @RequestBody MonedaModel nombre) {

        if (nombre == null) {
            return ResponseEntity.noContent().build();
        }

        String usuario = recuperarUsuario(token);

        if (usuario == null){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        UsuarioModel user = usuarioService.findByUsuario(usuario);

        if (user == null){
            return ResponseEntity.notFound().build();
        }

        CarteraModel cartera = user.getCartera();

        int i = 0;
        MonedaModel moneda = new MonedaModel();
        for (i = 0; i < user.getCartera().getMonedas().size(); i++){
            moneda = user.getCartera().getMonedas().get(i);
            if (moneda.getMoneda().equals(nombre.getMoneda()) && moneda.getCantidad() == 0){
                cartera = carteraService.deleteCartera(cartera, moneda);
            }
        }

        user.setCartera(cartera);
        usuarioService.save(user);

        return ResponseEntity.ok().build();
    }

    public String recuperarUsuario(String token){
        String usuario = "";
        try{
            usuario = jwtBuilder.recuperarUsuario(token);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return null;
        }
        System.out.println("usuario--> "+ usuario);

        return usuario;
    }

}
