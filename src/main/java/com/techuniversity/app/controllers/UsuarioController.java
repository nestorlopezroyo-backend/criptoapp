package com.techuniversity.app.controllers;


import com.techuniversity.app.components.JWTBuilder;
import com.techuniversity.app.enums.Rol;
import com.techuniversity.app.models.UsuarioModel;
import com.techuniversity.app.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;


@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private JWTBuilder jwtBuilder;

    public UsuarioService getUsuarioService() {
        return usuarioService;
    }

    public void setUsuarioService(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @GetMapping(path = "/", headers = {"Authorization"})
    public ResponseEntity<List<UsuarioModel>> getUsuarios(@RequestHeader("Authorization") String token, @RequestParam(defaultValue = "-1") String page){

        String usuario = recuperarUsuario(token);
        if (usuario == null){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        UsuarioModel user = usuarioService.findByUsuario(usuario);

        if(user == null){
            return ResponseEntity.notFound().build();
        }

        int iPage = Integer.parseInt(page);
        if (user.getRol().equals(Rol.ADMIN)) {
            if (iPage == -1) {
                List<UsuarioModel> lista = this.getUsuarioService().findAll();
                if (lista == null) {
                    return ResponseEntity.noContent().build();
                } else {
                    return ResponseEntity.ok().body(lista);
                }
            } else {
                List<UsuarioModel> lista = this.getUsuarioService().findPaginado(iPage);
                if (lista == null) {
                    return ResponseEntity.noContent().build();
                } else {
                    return ResponseEntity.ok().body(lista);
                }
            }
        }else{
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @GetMapping("/{user}")
    public ResponseEntity<UsuarioModel> getUsuarioId(@RequestHeader("Authorization") String token, @PathVariable String user){
        String usuario = recuperarUsuario(token);
        if (usuario == null){
            System.out.println("null usuario");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        UsuarioModel usu = usuarioService.findByUsuario(user);

        if (usu == null){
            System.out.println("null usu");
            return ResponseEntity.notFound().build();
        }

        UsuarioModel usuToken = usuarioService.findByUsuario(usuario);
        if (usuToken == null){
            System.out.println("null usutoken");
            return ResponseEntity.notFound().build();
        }

        if (user.equals(usuario)) {
                return ResponseEntity.ok().body(usu);
        }else {
            System.out.println(usuToken.getRol().toString());
            if (usuToken.getRol().equals(Rol.ADMIN)){
                return ResponseEntity.ok().body(usu);
            }else{
                System.out.println("accedemos como cliente a otro usuario");
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }
    }


    @PostMapping("/")
    public ResponseEntity<UsuarioModel> insertUsuario(@RequestBody List<UsuarioModel> nuevos){
        if (nuevos == null) {
            return ResponseEntity.noContent().build();
        } else {
            for (UsuarioModel usuario : nuevos){
                this.getUsuarioService().save(usuario);
            }
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
    }

    @PutMapping("/")
    public ResponseEntity<UsuarioModel> updateUsuario(@RequestBody UsuarioModel nuevo){
        UsuarioModel usu = this.getUsuarioService().save(nuevo);
        if (usu == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().build();
        }
    }

    @DeleteMapping("/")
    public ResponseEntity<UsuarioModel> deleteUsuario(@RequestBody UsuarioModel usuario){
        this.getUsuarioService().deleteUsuario(usuario);
        return ResponseEntity.ok().build();
    }

    public String recuperarUsuario(String token){
        String usuario = "";
        try{
            usuario = jwtBuilder.recuperarUsuario(token);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return null;
        }
        System.out.println("usuario--> "+ usuario);

        return usuario;
    }

}