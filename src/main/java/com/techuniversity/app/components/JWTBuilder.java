package com.techuniversity.app.components;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.ErrorCodes;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.security.auth.message.AuthException;

@Component
public class JWTBuilder {
    @Value("${jwt.user}")
    private String jwtUser;
    @Value("${jwt.password}")
    private String jwtPassword;
    @Value("${jwt.expiry}")
    private float jwtExpiry;
    RsaJsonWebKey rsaJsonWebKey;

    public JWTBuilder() {}

    public String generateToken(String userId, String password, String roles){
        String jwtIssuer = jwtUser + jwtPassword;
        try{
            JwtClaims jwtClaims = new JwtClaims();
            jwtClaims.setIssuer(jwtIssuer);
            jwtClaims.setExpirationTimeMinutesInTheFuture(jwtExpiry);
            jwtClaims.setAudience("ALL");
            jwtClaims.setStringClaim("groups", roles);  //Valores string a guardar extras
            jwtClaims.setGeneratedJwtId(); //Genera un ID para el token
            jwtClaims.setIssuedAtToNow(); //Hora actual para validaciones
            jwtClaims.setSubject("AUTHTOKEN");
            jwtClaims.setClaim("userID", userId);
            jwtClaims.setClaim("password", password);
            jwtClaims.setClaim("roles", roles);
            System.out.println("Claims OK");
            JsonWebSignature jws = new JsonWebSignature();
            jws.setPayload(jwtClaims.toJson());
            jws.setKey(rsaJsonWebKey.getRsaPrivateKey());        //clave privada
            jws.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId());  //clave publica
            jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
            System.out.println("Key OK");
            return jws.getCompactSerialization(); //devuelve el token


        } catch (JoseException ex){
            ex.printStackTrace();
            return null;
        }
    }
    public JwtClaims generateParseToken(String token) throws Exception {
        String jwtIssuer = jwtUser + jwtPassword;
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setRequireExpirationTime()
                .setSkipSignatureVerification()
                .setAllowedClockSkewInSeconds(60)
                .setRequireSubject()
                .setExpectedIssuer(jwtIssuer)
                .setExpectedAudience("ALL")
                .setExpectedSubject("AUTHTOKEN")
                .setVerificationKey(rsaJsonWebKey.getKey())
                .setJwsAlgorithmConstraints(new AlgorithmConstraints(
                        AlgorithmConstraints.ConstraintType.WHITELIST, AlgorithmIdentifiers.RSA_USING_SHA256
                )).build();

        try{
            JwtClaims jwtClaims = jwtConsumer.processToClaims(token);
            return jwtClaims;
        } catch (InvalidJwtException e){
            try{
                if (e.hasExpired()){
                    throw new Exception("Caducada en: " + e.getJwtContext().getJwtClaims().getExpirationTime());
                }

                if(e.hasErrorCode(ErrorCodes.AUDIENCE_INVALID)){
                    throw new Exception("Audiencia incorrecta: "+ e.getJwtContext().getJwtClaims().getAudience());
                }
                throw new AuthException(e.getMessage());
            } catch (MalformedClaimException innerEx){
                throw  new AuthException("Mal Formada");
            }
        }
    }
    public String recuperarRol (String token) throws Exception{
        String roles = null;
        JwtClaims jwtClaims = generateParseToken(token);
        roles = jwtClaims.getClaimValue("roles").toString();
        return roles;
    }

    public String recuperarUsuario (String token) throws Exception{
        String usuario = null;
        JwtClaims jwtClaims = generateParseToken(token);
        usuario = jwtClaims.getClaimValue("userID").toString();
        return usuario;
    }

    @PostConstruct
    public void init(){
        try{
            rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);
            rsaJsonWebKey.setKeyId(jwtPassword);

        }catch (JoseException ex){
            ex.printStackTrace();
        }
    }
}
