package com.techuniversity.app.models;

import com.techuniversity.app.enums.Rol;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "criptoapp")
public class UsuarioModel {

    @Id
    @NotNull
    private String id;
    private String password;
    private String usuario;
    private String nombreCompleto;
    private String documentoId;
    private Rol rol;
    private CarteraModel cartera;

    public UsuarioModel(){

    }

    public UsuarioModel(@NotNull String id, String password, String usuario, String nombreCompleto, String documentoId, Rol rol, CarteraModel cartera) {
        this.id = id;
        this.password = password;
        this.usuario = usuario;
        this.nombreCompleto = nombreCompleto;
        this.documentoId = documentoId;
        this.rol = rol;
        this.cartera = cartera;
    }

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDocumentoId() {
        return documentoId;
    }

    public void setDocumentoId(String documentoId) {
        this.documentoId = documentoId;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public CarteraModel getCartera() {
        return cartera;
    }

    public void setCartera(CarteraModel cartera) {
        this.cartera = cartera;
    }

    @Override
    public String toString() {
        return "UsuarioModel{" +
                "id='" + id + '\'' +
                ", password='" + password + '\'' +
                ", usuario='" + usuario + '\'' +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                ", documentoId='" + documentoId + '\'' +
                ", rol=" + rol +
                ", cartera=" + cartera +
                '}';
    }
}