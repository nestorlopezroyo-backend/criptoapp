package com.techuniversity.app.models;


import java.util.ArrayList;
import java.util.List;

public class CarteraModel {
    private List<MonedaModel> monedas;

    public CarteraModel(List<MonedaModel> monedas) {
        this.monedas = monedas;
    }

    public CarteraModel() {
    }

    @Override
    public String toString() {
        return "CarteraModel{" +
                "monedas=" + monedas +
                '}';
    }

    public List<MonedaModel> getMonedas() {
        return monedas;
    }

    public void setMonedas(List<MonedaModel> monedas) {
        this.monedas = monedas;
    }
}
