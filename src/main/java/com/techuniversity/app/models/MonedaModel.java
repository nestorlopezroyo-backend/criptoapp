package com.techuniversity.app.models;

import com.techuniversity.app.services.PrecioService;
import org.springframework.beans.factory.annotation.Autowired;

public class MonedaModel {

    private String moneda;
    private double cantidad;
    private double cambio;

    public MonedaModel(String moneda, double cantidad, double cambio) {
        this.moneda = moneda;
        this.cantidad = cantidad;
        this.cambio = cambio;
    }

    public MonedaModel() {
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getCambio() {
        return cambio;
    }

    public void setCambio(double cambio) {
        this.cambio = cambio;
    }

    @Override
    public String toString() {
        return "MonedaModel{" +
                "moneda='" + moneda + '\'' +
                ", cantidad=" + cantidad +
                ", cambio=" + cambio +
                '}';
    }
}
